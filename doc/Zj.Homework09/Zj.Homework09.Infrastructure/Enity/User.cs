﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zj.Homework09.Infrastructure.Entity
{
    /// <summary>
    /// 用户
    /// </summary>
    public class User:BaseEntity
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string Mail { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Pwd { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadImg { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime LastLoginTime { get; set; }

        /// <summary>
        /// 分组
        /// </summary>
        public int UserGroup { get; set; }
    }
}
