﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zj.Homework09.Infrastructure._Attribute
{
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Field|AttributeTargets.Property,AllowMultiple =false,Inherited =false)]
    public class RemarkAttribute:Attribute
    {
        public string Text { get;private set; }

        public RemarkAttribute(string text)
        {
            this.Text = text;
        }
    }
}
