﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zj.Homework09.Infrastructure._Attribute;

namespace Zj.Homework09.Infrastructure.Enums
{
    public enum UserState
    {
        [Remark("离线")]
        OffLine = 0,
        [Remark("在线")]
        Online = 1,
    }
}
