﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zj.Homework09.Infrastructure._Attribute;

namespace Zj.Homework09.Infrastructure.Enums
{
    public enum UserGroupEnum
    {
        [Remark("高级班")]
        Advanced = 1,
        [Remark("初级班")]
        Primary = 0,
    }
}
