﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace Zj.Homework09.WebApp.Controllers
{
    public class HiChatApiController : ApiController
    {
        private static List<WebSocket> _socketList = new List<WebSocket>(); /*存储当前所有连接*/
        private CancellationToken ct = new CancellationToken();/*信号量*/
        [HttpGet]
        public HttpResponseMessage ProcessMessage()
        {
            var ctx = HttpContext.Current;
            var req = ctx.Request;
            var res = ctx.Response;
            var user = req.QueryString["user"];

            var b = ctx.IsWebSocketRequest;/*是否是WebSocket请求*/
            if (b)
            {
                ctx.AcceptWebSocketRequest(ASPNetWebSocketFunc);
            }
            return new HttpResponseMessage(HttpStatusCode.SwitchingProtocols);
        }
        private async System.Threading.Tasks.Task ASPNetWebSocketFunc(System.Web.WebSockets.AspNetWebSocketContext arg)
        {
            var ws = arg.WebSocket;/*得到WebSocket对象*/
            _socketList.Add(ws);
            while (true)
            {
                var buffer = new ArraySegment<byte>(new byte[1024]);
                var receiveRt = await ws.ReceiveAsync(buffer, ct);/*异步接收消息*/
                if (receiveRt.MessageType == WebSocketMessageType.Close)
                {
                    _socketList.Remove(ws);
                    await ws.CloseAsync(WebSocketCloseStatus.Empty, string.Empty, ct);
                    break;
                }
                if (ws.State == WebSocketState.Open)
                {
                    string msg = Encoding.UTF8.GetString(buffer.Array, 0, receiveRt.Count);
                    var recvBytes = Encoding.UTF8.GetBytes(msg);
                    var bufferSend = new ArraySegment<byte>(recvBytes);

                    foreach (var webSocket in _socketList)/*消息广播*/
                    {
                        if (ws != webSocket)
                        {
                            await webSocket.SendAsync(bufferSend, WebSocketMessageType.Text, true, ct);
                            await webSocket.SendAsync(bufferSend, WebSocketMessageType.Binary, true, ct);
                        }
                    }
                }
            }
        }
    }
}
