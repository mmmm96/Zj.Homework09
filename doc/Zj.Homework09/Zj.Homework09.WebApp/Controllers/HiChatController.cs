﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using System.Net;
using System.Net.Http;
using Zj.Homework09.ViewModel.Home;

namespace Zj.Homework09.WebApp.Controllers
{
    public class HiChatController : Controller
    {
        // GET: HiChat
        public ActionResult Index(string mail,string nick)
        {
            UserViewModel user = new UserViewModel { Mail = mail, Nick = nick };

            return View(user);
        }  
    }
}