﻿(function ($) {
    var index = {
        init: function () {
            var me = this;
            me.render();
            me.bind();
        },
        render: function () {
            var me = this;
            me.mail = $('#txtMail');/*邮箱*/
            me.nick = $('#txtNick');/*昵称*/
            me.test = $('#btnTest');
        },
        bind: function () {
            var me = this;
            me.test.on('click', function () {
                var mail = $.trim(me.mail.val());
                var nick = $.trim(me.nick.val());
                location.href = '/HiChat/Index?mail=' + mail + '&nick=' + nick;
            });
        }
    };
    $(function () {
        index.init();
    });
})(jQuery)