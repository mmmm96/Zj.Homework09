﻿(function ($) {
    var index = {
        init: function () {
            var me = this;
            me.render();
            me.bind();
        },
        datas: {
            url: "api/HiChatApi/ProcessMessage"/*WebSocket连接的后台url*/,
            //url: "HiChat/ProcessRequest"/*WebSocket连接的后台url*/,
            //url: "HiChatHandler.ashx"/*WebSocket连接的后台url*/,
            
        },
        render: function () {
            var me = this;
            me.tab = $('.js-tab');/*tab切换*/
            me.li = $('.js-li-item');
            me.item = $('.js-users');
            me.lt = $('.js-lt');
            me.tabitem = $('.js-tab-item');
            me.chatClose = $('.js-div-close');
            me.chat = $('.js-chat');
            me.time = $('.js-time span');
            me.send = $('#btnSend');/*消息发送*/
            me.msg = $('#textMessage');
            me.msgShow = $('.js-chat-body');/*消息显示*/
            me.nick = $('.js-user-nickname');/*昵称*/
            me.mail = $('.js-user-signature');/*邮箱*/
            me.voice = $('#btnVoice');/*说话*/
            me.audio = null;
            
            me.ws = me.method.createServer(me.msgShow, me.datas.url);/*WebSocket*/
        },
        bind: function () {
            var me = this;
            document.addEventListener('plusready', function () {
                me.audio = plus.audio.getRecorder();
            }, false)
            me.time.text(me.method.getTime());
            setInterval(function () {
                me.time.text(me.method.getTime());
            }, 1000);/*时间更新*/
            me.tabitem.eq(0).siblings('.js-tab-item').hide();/*先隐藏除第一个以外的tab*/
            me.tab.on('click', function (e) {
                $(this).siblings('.js-tab').toggleClass('js-li-active');
                $(this).toggleClass('js-li-active');

                var id = me.tab.filter('.js-li-active').attr('href').substr(1);
                $('#' + id).toggle();
                $('#' + id).siblings('.js-tab-item').toggle();
            });
            me.li.on('click', function () {
                var index = $(this).index();
                me.item.eq(index).slideToggle();
            });
            me.chatClose.on('click', function () {
                me.chat.fadeOut();
            });
            me.send.on('click', function () {
                var nick = me.nick.text();/*昵称*/
                var msg = $.trim(me.msg.val());
                me.method.sendMsg(me.ws, nick+','+msg, me.msg);/*本端发送消息*/

                var msgHtml = me.method.getChatInfoHtml('right',nick, msg);
                me.msgShow.append(msgHtml);/*并将本端发送的消息显示到右侧*/
            });
            me.voice.on('click', function () {
                if (me.audio==null) {
                    console.log('audio not ready');
                } else {
                    console.log('audio ready');
                    me.audio.record('Assets/', function () {
                        console.log('success')
                    }, function () {
                    console.log('record failed')});
                }
                //me.ws.send($('#fileImg').val());
            });
        },
        method: {
            /*当前时间*/
            getTime: function () {
                var time = new Date();
                return time.getHours() + ':' + time.getMinutes();
            },
            /*创建WebSocket服务器*/
            createServer: function ($obj,url) {
                var me = this;
                var nick = index.nick;
                if ("WebSocket" in window) {
                    if (!ws) {
                        url = 'ws://' + location.hostname + ':' + location.port + '/' + url ;
                        var ws = new WebSocket(url);
                        ws.onopen = function () {
                            var tint = me.getTintHtml(nick.text() +'加入了群聊');
                            $obj.append(tint);
                            console.log('connection open');
                        };
                        ws.onmessage = function (e) {
                             var data = e.data;
                             if (typeof (data)=='string') {
                                 var nick = data.split(',')[0];
                                 var msg = data.split(',')[1];
                                 var msgHtml = me.getChatInfoHtml('left', nick, msg);
                                 $obj.append(msgHtml);
                             } else {
                                 var fr = new FileReader();
                                 fr.onload = function (event) {
                                     if (fr.readyState == FileReader.DONE) {
                                         var url = event.target.result;
                                         $('#img').attr('src', url);
                                     }
                                     
                                 }
                                 fr.readAsDataURL(data);
                             }   
                        };
                        ws.onerror = function (error) {
                            console.log(error);
                        };
                        ws.onclose = function () {
                            
                            var tint = me.getTintHtml(nick.text()+'离开了群聊');
                            $obj.append(tint);
                        };
                        return ws;
                    }
                } else {
                    alert("浏览器不支持WebSocket");
                }
            },
            /*ws发送消息*/
            sendMsg: function (ws, msg, $msg) {
                if (ws.readyState == WebSocket.OPEN) {
                    ws.send(msg);
                    $msg.val('');/*清空value*/
                    $msg.focus();
                }
            },
            /*拼接消息html*/
            getChatInfoHtml: function (direction,nick, msg) {
                if (direction != 'left' && direction != 'right') {
                    return '';
                }
                var str = '<div class="col-sm-12"><div class="js-msg-' + direction + ' clearfix text-black"><div class="js-msg-img pull-' + direction + '">'
                          + '<img src=""  width="50" height="50" /></div><div class="js-msg-text pull-' + direction + '"><div class="js-msg-nick text-' + direction + '">'+nick+'</div>'
                           + '<div class="js-msg-info bg-white">' + msg + '</div></div></div></div>';
                return str;
            },
            /*拼接提示消息html*/
            getTintHtml:function(tint) {
                var str = '<div class="js-time text-white text-center"><span>' + tint + '</span></div>';
                return str;
            }
        }
    };
    $(function () {
        index.init();
    });
})(jQuery)