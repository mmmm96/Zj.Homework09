﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Zj.Homework09.WebApp
{
    [HubName("hubChat")]
    public class HubChat : Hub
    {
        //public void Hello()
        //{
        //    Clients.All.hello();
        //}
        [HubMethodName("send")]
        public void Send(string name,string message)
        {
            /*向所有用户广播消息*/
            Clients.All.broadcastMessage(name,message);
        }
    }


}