﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Zj.Homework09.WebApp.Startup))]
namespace Zj.Homework09.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();/*配置SignalR*/
        }
    }
}
